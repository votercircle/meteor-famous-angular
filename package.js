Package.describe({
  name: 'votercircle:meteor-famous-angular',
  summary: 'Famous-Angular packaged for use with Meteor apps',
  version: '1.0.0',
  git: 'git@bitbucket.org:votercircle/meteor-famous-angular.git'
});

Package.onUse(function(api) {
  api.versionsFrom('1.0');
  api.use('urigo:angular@0.8.8', 'client');
  api.use('mjn:famous@0.3.5', 'client');
  api.addFiles('famous-angular.js', 'client');
  api.addFiles('famous-angular.css', 'client');
});
